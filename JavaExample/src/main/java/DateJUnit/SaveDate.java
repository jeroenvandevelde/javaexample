package DateJUnit;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Jeroen Vandevelde on 16/09/2016.
 */
public class SaveDate {
    private Date date;
    private LocalDateTime localDateTime;
    private org.joda.time.LocalDateTime localDateTimeJoda;

    SaveDate() {

    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime (LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDateTime(Date date) {
        this.date = date;
    }

    public org.joda.time.LocalDateTime getLocalDateTimeJoda() {
        return localDateTimeJoda;
    }

    public void setLocalDateTimeJoda(org.joda.time.LocalDateTime localDateTimeJoda) {
        this.localDateTimeJoda = localDateTimeJoda;
    }
}
