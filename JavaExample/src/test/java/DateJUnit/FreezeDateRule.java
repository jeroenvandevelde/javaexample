package DateJUnit;

import org.joda.time.DateTimeUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Jeroen Vandevelde on 21-2-2016.
 */
public class FreezeDateRule extends TestWatcher {

    public static FreezeDateRule create() {
        return new FreezeDateRule();
    }

    private FreezeDateRule(){}

    @Override
    protected void finished(Description description) {
        super.finished(description);
        DateTimeUtils.setCurrentMillisSystem();
    }

    public void freezeTime(Date date) {
        DateTimeUtils.setCurrentMillisFixed(date.getTime());
    }
}
