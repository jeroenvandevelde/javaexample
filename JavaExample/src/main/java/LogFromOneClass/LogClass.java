package LogFromOneClass;
import org.apache.log4j.Logger;

/**
 * Created by Jeroen Vandevelde on 31/10/2016.
 */

public class LogClass {
    final static Logger logger = Logger.getLogger(LogClass.class); //Creating a Log4JLogger

    public static void main(String[] args){
        /*
        This class uses the abc appender configured in the resources/log4j.properties
        Could be useful when you quickly want to send logs from one class to one file
        so the logs doesn't get scattered by other logs.
         */

        logger.debug("This is a debug message.");
        logger.info("This is an info message.");
        logger.warn("This is a warn message.");
        logger.fatal("This is a fatal message.");
        logger.error("This is an error message.");
    }
}
