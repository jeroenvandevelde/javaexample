package DateJUnit;

import org.joda.time.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

import static java.lang.Thread.sleep;
import static org.junit.Assert.*;

/**
 * Created by vandej21 on 12/09/2016.
 */
public class SaveDateTest {
    /*
    * Dummy example of a JUnit test with dates.
    * Making use of a rule to "freeze" and "release" the DateJUnit and time.
    * A rule can be reused in different test classes.
     */

    @Rule
    public FreezeDateRule freezeDateRule = FreezeDateRule.create();

    public SaveDate saveDate = new SaveDate();

    /*
        Java starts counting from January 1, 1970 00:00:00.000 GMT and counts the time until the date you want.
        Because of this if your JUnit takes too long to get to the second new Date() your dates won't be equal.
        new Date() = Now();
     */
    @Test
    public void testUnequalSetDate() throws InterruptedException {
        Date date = new Date();

        sleep(50);

        assertNotEquals(date, new Date());
    }

    /*
        Java 8+
     */
    @Test
    public void testSetDateTimeJ8() throws InterruptedException {
        Clock c = Clock.fixed(Instant.now(), ZoneId.of("UTC"));

        LocalDateTime localDateTime = LocalDateTime.now(c);

        saveDate.setLocalDateTime(localDateTime);

        Thread.sleep(200);

        assertEquals(LocalDateTime.now(c), saveDate.getLocalDateTime());
    }

    /*
    The 2 dates are equal because the "joda date" (and not the java date) is freezed.
    Because of this the dates have the same time away from January 1, 1970 00:00:00.000 GMT.
     */
    @Test
    public void testSetDate() throws InterruptedException {
        org.joda.time.LocalDateTime d = new org.joda.time.LocalDateTime(1995,12,2,6,6,6);

        freezeDateRule.freezeTime( d.toDate());

        sleep(500);

        saveDate.setLocalDateTimeJoda(d);

        org.joda.time.LocalDateTime ds = org.joda.time.LocalDateTime.now();

        assertEquals(d.toDate(), ds.toDate());
    }

    /*
    The 2 dates aren't equal because the "joda date" (and not the java date) isn't freezed.
    Because of this the dates don't have the same time away from January 1, 1970 00:00:00.000 GMT.
     */
    @Test
    public void testSetDate2() throws InterruptedException {
        org.joda.time.LocalDateTime d = new org.joda.time.LocalDateTime(1995,12,2,6,6,6);

        //freezeDateRule.freezeTime( d.toDate());

        sleep(500);

        saveDate.setLocalDateTimeJoda(d);

        org.joda.time.LocalDateTime ds = org.joda.time.LocalDateTime.now();;

        assertNotEquals(d.toDate(), ds.toDate());
    }
}